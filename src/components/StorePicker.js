import React, { Fragment } from 'react';
import { getFunName } from '../helpers';

class StorePicker extends React.Component {
  myInput = React.createRef();

  goToStore = event => {
    event.preventDefault();
    // get text from input
    //console.log(this.myInput.current.defaultValue);
    const storeName = this.myInput.current.value;
    // change the page to /store/input
    this.props.history.push(`/store/${storeName}`);
  };
  render() {
    return (
      <Fragment>
        <form className="store-selector" onSubmit={this.goToStore}>
          <h2>Please, choose the store</h2>
          <input
            type="text"
            ref={this.myInput}
            required
            placeholder="Store name"
            defaultValue={getFunName()}
          />
          <button type="submit"> Visit store </button>
        </form>
      </Fragment>
    );
  }
}

export default StorePicker;
